# FP - DCGs for fp_main
DCGs predicates for different programming languages used to identify subprogram declarations.

See [fp_main](https://gitlab.com/cnngimenez/fp_main) repository.

# Adding a new language
In order to support other languages, there should be:

- A prolog file with the name of the language with the DCGs rules.
  - The only exception is `prologlang` which is for Prolog code.
- A `main_LANGUAGE(-Entry)//` where the entry should be unified with:
  - A pack/1 term with the package (or class) information.
  - A dec/3 term with the subprogram information.


- pack/1 is `pack(Name: codes)`.
- dec/3 is `dec(Name: codes, Params: codes, Desc: codes)`.

The line number and the file name is added by the fp_main module.

## Example
For example, to support the Ada language there are:

- An ada.pl file.
- A `main_ada//1` DCG rule that recognizes:
  - Ada subprograms (procedures and functions, see `subprog_ada//1`.
  - Ada packages names (see `pack_ada//1`).

# Running Tests

Load the modules inside the tests directory and run the tests using `run_tests.`.


See [Prolog Unit Tests package documentation](http://www.swi-prolog.org/pldoc/doc_for?object=section(%27packages/plunit.html%27)) for more information.
