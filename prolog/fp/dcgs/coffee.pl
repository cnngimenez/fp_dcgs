/*   coffee
     Author: cnngimenez.

     Copyright (C) 2016 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     21 oct 2016
*/


:- module(coffee, [
	      subprog_coffee//1
	  ]).
/** <module> coffee: coffee

CoffeeScript
*/

:- use_module(library(lists)).
:- ensure_loaded(library(dcg/basics)).
:- use_module(commondcg).

id_sym([S]) --> digit(S).
id_sym([S]) --> letter(S).
id_sym(`_`) --> "_".

ident(S) --> id_sym(S).
ident(NameL) --> id_sym(S), ident(Rest), {append(S, Rest, NameL)}.

comment_line(CL) --> "#", whites, string_without(`\n`, CL), blanks.

comments(DescL) --> comment_line(DescL).
comments(DescL) --> comment_line(CommentLine),
		    comments(Rest),!,
		    {append([CommentLine, `\n`, Rest], DescL)}.

subprog_coffee(dec(Name, Arg, Desc)) -->
    comments(DescL), blanks,
    ident(NameL), (whites ; []), ":", (whites ; []), "(", string(ArgL), ")", (whites ; []), "->",
    {string_codes(Name, NameL),
     string_codes(Arg, ArgL),
     string_codes(Desc, DescL)}.
subprog_coffee(dec(Name, Arg, "")) -->
    ident(NameL), (whites ; []), ":", (whites ; []), "(", string(ArgL), ")", (whites ; []), "->",
    {string_codes(Name, NameL),
     string_codes(Arg, ArgL)}.
