/*   commondcg
     Author: cnngimenez.

     Copyright (C) 2016 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     19 ago 2016
*/


:- module(commondcg, [
	      %% space//0, spaces//0,
	      letter//1,
	      word//0,
	      blanksnl//1,
	      remove_spaces//1
	  ]).
/** <module> commondcg: Common DCG rules.

Some useful rules that may be needed for every language.
*/

:- use_module(library(lists)).
:- ensure_loaded(library(dcg/basics)).

% No need blanks//0 do this for us.
%% space --> white.
%% space --> "\n".

%% spaces --> space.
%% spaces --> space, spaces.

word --> nonblanks(_), blank.

letter(C, [C|R], R) :-
    code_type(C, alpha).

blanksnl(0) --> white.
blanksnl(1) --> "\n".
blanksnl(S) --> "\n", blanksnl(S1), {S is S1 + 1}.
blanksnl(S) --> white, blanksnl(S).

nonempty_blanks --> (" "; "\n"; "\t"), nonempty_blanks, !.
nonempty_blanks --> " ".
nonempty_blanks --> "\n".
nonempty_blanks --> "\t".


/**
   remove_spaces(-Str) // is det.

Return an Str from the given string with only one space instead of \n, \t and
more than two.
*/
remove_spaces(Str) --> nonempty_blanks, nonblanks(C), { C\= [] }, !, remove_spaces(Str2),
		       {format(codes(Str), ' ~s~s', [C, Str2])}.
remove_spaces(Str) --> nonblanks(C), {C \= []}, !, remove_spaces(Rest),
		       {append(C, Rest, Str)}.
remove_spaces([]) --> nonempty_blanks, !.
remove_spaces([]) --> [].


