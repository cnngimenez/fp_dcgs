/*   elisp
     Author: cnngimenez.

     Copyright (C) 2018 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     02 abr 2018
*/


:- module(elisp, [
	      subprog_elisp//1,
	      pack_elisp//1,
	      main_elisp//1
	  ]).
/** <module> elisp: Elisp support for FP.


*/

:- use_module(library(lists)).
:- ensure_loaded(library(dcg/basics)).
:- use_module(commondcg).

elisp_args(Args) -->  "(", string_without(`)`, Args), ")", !.
funcname(Name) --> string_without(` \n\t`, Name), !.
comments(Desc) --> `"`, !,  string(Desc), `"`.

subprog_elisp(dec(Name, Arg, Desc)) -->
    "(", blanks, "defun", blanks, funcname(Name),
    blanks, elisp_args(Arg),
    blanks, comments(Desc), !.

subprog_elisp(dec(Name, Arg, [])) -->
    "(", blanks, "defun", blanks, funcname(Name),
    blanks, elisp_args(Arg).

pack_elisp(package(Name)) --> "(", blanks, "provide", blanks, "'", string_without(` \t\n\r)`, Name).

main_elisp(Entry) --> pack_elisp(Entry).
main_elisp(Entry) --> subprog_elisp(Entry).
