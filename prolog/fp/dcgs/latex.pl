/*   latex
     Author: cnngimenez.

     Copyright (C) 2016 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     23 ago 2016
*/


:- module(latex, [
	      subprog_latex//1
	  ]).
/** <module> latex: LaTeX DCGs


*/

:- use_module(library(lists)).
:- ensure_loaded(library(dcg/basics)).
:- use_module(commondcg).

/** id_symbol(?S).

true iff S is a symbol that can be part of a LaTeX identifier.

Remember that the `@` is a symbol that declares a private identifier, usually
they are definitions not important and hence is ignored from the matching
symbols.
*/
id_symbol(`_`) --> "_".
%% id_symbol(`@`) --> "@".
id_symbol([S]) --> letter(S).
id_symbol([S]) --> digit(S).

id_symbols(S) --> id_symbol(S).
id_symbols(S1) --> id_symbol(S), id_symbols(S2), {append(S, S2, S1)}.

identifier(Name) --> "\\", id_symbols(S), {append(`\\`, S, Name)}.

/** subprog_latex(-Dec, ?List, ?Rest).

Matchs the new macro definitions from List and match Rest with the rest of the
nonmatching char list. Dec is a dec/3 declaration with the Name and Args
definitions.

Comments on dec/3 is not supported on LaTeX due to the diversity of doing so 
(no conventions on in-site comments!).
*/
subprog_latex(dec(Name, "", "")) --> "\\newcommand{", identifier(NameL), "}{",
				 {string_codes(Name,NameL)}.
subprog_latex(dec(Name, Args, "")) --> "\\newcommand{", identifier(NameL), "}[", digits(ArgsL), "]",
				   {string_codes(Name, NameL), string_codes(Args, ArgsL)}.
subprog_latex(dec(Name, "", "")) --> "\\def", identifier(NameL), blank,
				 {string_codes(Name,NameL)}.
