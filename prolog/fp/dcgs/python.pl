/*   python
     Author: cnngimenez.

     Copyright (C) 2017 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     27 mar 2017
*/


:- module(python, [
	      subprog_python//1
	  ]).
/** <module> python: Python DCG Specification

This are DCG implementation for recognize Python definitions.
*/

:- use_module(library(lists)).
:- ensure_loaded(library(dcg/basics)).
:- use_module(commondcg).

id_sym([S]) --> digit(S).
id_sym([S]) --> letter(S).
id_sym(`_`) --> "_".

ident(NameL) --> id_sym(S), ident(Rest),!, {append(S, Rest, NameL)}.
ident(S) --> id_sym(S), !.
			  
comments(DescL) --> blanks, "'''", string(DescL), blanks, "'''",!.
comments(DescL) --> blanks, "\"\"\"", string(DescL), blanks, "\"\"\"",!.

/**
   args(-Args:list) // is det.

Parse the python argument without parenthesis.

@param Args The arguments parsed with double blanks (newlines, tabs or spaces) translated into only one.
*/
args(Args) --> string_without(")", A),
	       {remove_spaces(Args, A, [])}.

nom_proc(NameL) --> blanks, ident(NameL), (blanks ; []).

arg_proc(ArgL) --> "(", args(ArgL), ")", (string_without(":", _); []), ":",!.
arg_proc([]) --> "(", blanks, ")", (string_without(":", _); []), ":",!.

comments_proc(DescL) --> blanks, comments(DescL),!.
comments_proc([]) --> blanks.
/**
   subprog_python(-DecPred:term)

Recognize Python defs.
*/
subprog_python(dec(Name, Arg, Desc)) -->
    % blank : require at least a blank (\n,\r, \t, or space).
    % blanks: *zero* or more blank.
    "def", blank, nom_proc(NameL),
    arg_proc(ArgL),
    comments_proc(DescL),!,
    {string_codes(Name, NameL),
     string_codes(Arg, ArgL),
     string_codes(Desc, DescL)}.
