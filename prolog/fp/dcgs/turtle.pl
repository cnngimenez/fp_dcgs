/*   turtle
     Author: cnngimenez.

     Copyright (C) 2016 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     05 oct 2016
*/


:- module(turtle, [
	      subprog_turtle//1
	  ]).
/** <module> turtle: RDF Turtle DCGs for parsing RDF Schemma


*/

% :- ensure_loaded(library(prosqlite)).
% :- ensure_loaded(library(db_facts)).
% :- use_module(...).


:- use_module(library(lists)).
:- ensure_loaded(library(dcg/basics)).
:- use_module(commondcg).

suffix_sym([S]) --> nonblank(S), {S \= 34}.
suffix_sym([S]) --> blank(S).

suffix_str(S) --> suffix_sym(S).
suffix_str(Suffix) --> suffix_sym(S), suffix_str(Rest),
		      {append(S, Rest, Suffix)}.

prefix_sym([S]) --> nonblank(S), {S \= 58}.
ttlprefix(S) --> prefix_sym(S).
ttlprefix(Prefix) --> prefix_sym(S), ttlprefix(Rest),
		      {append(S, Rest, Prefix)}.
ttlsuffix(Suffix) --> "\"", suffix_str(Suffix), "\"".
ttlsuffix(Suffix) --> nonblanks(Suffix).

ttl_url(Url) --> "<", nonblanks(Url), ">".
ttl_url(Url) --> ttlprefix(Prefix), ":", ttlsuffix(Suffix),
		 {append([Prefix, `:`, Suffix], Url)}.

rdfcomment(Comment) --> "rdfs:comment", blanks, "\"", string(Comment), "\"".

ttl_body(Comment) --> string(_), ";", blanks, rdfcomment(Comment), (blanks ; []), ("."; ";"),!.
ttl_body(``) --> [].

subprog_turtle(dec(Name, Arg, Desc)) -->
    ttl_url(NameL), blanks, "a", blanks, ttl_url(ArgL),ttl_body(DescL),
    {string_codes(Name, NameL),
     string_codes(Arg, ArgL),
     string_codes(Desc, DescL)}.
