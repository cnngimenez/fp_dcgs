/*   test_ada.pl
     Author: Giménez, Christian.

     Copyright (C) 2020 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 nov 2020
*/

:- module(test_ada, []).

:- use_module('../prolog/fp/dcgs/ada').

:- begin_tests(ada).

test(ada_arguments) :-
    Str = "(Par1: String)",
    string_codes(Str, Codes),
    
    phrase(ada:arguments(Args), Codes, Rest),
    
    Rest == [],
    string_codes("Par1: String", Args).

test(function_with_comments) :-
    Comment = "This is a comment",
    Dec = "--  This is a comment
function My_Function(Par1: String) return String;
",
    Name = "My_Function",
    Params = "Par1: String",
    Rest = " return String;\n",

    string_codes(Comment, CommentC),
    string_codes(Dec, DecC),
    string_codes(Name, NameC),
    string_codes(Params, ParamsC),
    string_codes(Rest, RestC),
    
    phrase(main_ada(Result), DecC, ResultRest),

    ResultRest == RestC,
    Result == dec(NameC, ParamsC, CommentC).

test(function) :-
    Str = `function My_Function(Par1: String) return String;`,
    phrase(main_ada(Result), Str, Rest),
    Rest == ` return String;`,
    Result == dec(`My_Function`, `Par1: String`, []).

test(procedure_with_comments) :-
    Comment = "This is a comment",
    Dec = "--  This is a comment
procedure My_Procedure(Par1: String);
",
    Name = "My_Procedure",
    Params = "Par1: String",
    Rest = ";\n",

    string_codes(Comment, CommentC),
    string_codes(Dec, DecC),
    string_codes(Name, NameC),
    string_codes(Params, ParamsC),
    string_codes(Rest, RestC),
    
    phrase(main_ada(Result), DecC, ResultRest),

    ResultRest == RestC,
    Result == dec(NameC, ParamsC, CommentC).

test(procedure) :-
    Str = `procedure My_Procedure(Par1: String);`,
    phrase(main_ada(Result), Str, Rest),
    Rest == `;`,
    Result == dec(`My_Procedure`, `Par1: String`, []).

test(package) :-
    Str = `package My_Package is`,
    phrase(main_ada(Result), Str, []),
    Result == package(`My_Package`).

test(package_body) :-
    Str = `package body My_Package is`,
    phrase(main_ada(Result), Str, []),
    Result == package(`My_Package`).


:- end_tests(ada).
