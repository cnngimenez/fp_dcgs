/*   test_elisp.pl
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     8 dic 2020
*/

:- module(test_elisp, []).

:- use_module('../prolog/fp/dcgs/elisp').

:- begin_tests(tests_name, []).

test(function_without_comments) :-
    Str = "(defun sachac-dir-git ()
  \"Return the complete git path.\"
  (concat sachac-data-directory \"/\" sachac-git-dirname) ) ;; defun
",
    string_codes(Str, Codes),

    phrase(subprog_elisp(Entry), Codes, _Rest_Codes),

    Entry = dec(`sachac-dir-git`, ``, `Return the complete git path.`).


test(function_without_arguments) :-
    Str = "(defun an-elisp-function () (interactive) (somethin-else))",
    string_codes(Str, Codes),

    phrase(subprog_elisp(Entry), Codes, _Rest_Codes),

    Entry = dec(`an-elisp-function`, [], []).

test(function_without_comments) :-
    Str = "(defun an-elisp-function (args) (interactive) (somethin-else))",
    string_codes(Str, Codes),

    phrase(subprog_elisp(Entry), Codes, _Rest_Codes),

    Entry = dec(`an-elisp-function`, `args`, []).

test(function_with_comments) :-
    Str = "(defun an-elisp-function (args) \"this is a test\" (something))",
    string_codes(Str, Codes),

    phrase(subprog_elisp(Entry), Codes, _Rest_Codes),
   
    Entry = dec(`an-elisp-function`, `args`, `this is a test`).

test(pack_elisp) :-
    Str = "(provide 'test-package)",
    string_codes(Str, Codes),

    phrase(pack_elisp(Entry), Codes, _Rest_Codes),
   
    Entry = package(`test-package`).
    

:- end_tests(tests_name).
