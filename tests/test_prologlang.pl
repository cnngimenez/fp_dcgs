/*   test_prologlang.pl
     Author: Giménez, Christian.

     Copyright (C) 2020 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 nov 2020
*/

:- module(test_prologlang, []).

:- use_module('../prolog/fp/dcgs/prologlang').

:- begin_tests(prologlang).

test(head_without_args) :-
    Str = `predicate.`,
    phrase(main_prologlang(Result), Str, Rest),
    Rest == [],
    Result == dec(`predicate`, [], []).

test(head_with_args) :-
    Str = `predicate(Arg1).`,
    phrase(main_prologlang(Result), Str, Rest),
    Rest == [],
    Result == dec(`predicate`, `Arg1`, []).

%% Not yet implemented:
%% test(package) :-
%%     Str = `:- module(the_package).`,
%%     phrase(main_ada(Result), Str, Rest),
%%     Rest == [],
%%     Result == package(`the_package`).

%% Not yet implemented:
%% test(package2) :-
%%     Str = `:- module(the_package, []).`,
%%     phrase(main_ada(Result), Str, Rest),
%%     Rest == [],
%%     Result == package(`the_package`).
    

:- end_tests(prologlang).
