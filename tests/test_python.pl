/*   python
     Author: cnngimenez.

     Copyright (C) 2017 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     05 abr 2017
*/


:- module(test_python, []).
/** <module> python: Tests for FP - Python DCGs.


*/


:- begin_tests(python).
:- use_module('../prolog/dcgs/python').

test(subprog_python) :-
    subprog_python(
	dec("foo", " arg1: 'type', arg2: 'type2'", "\n\tcomment\n    \tmorecomments"),
	`def foo (  arg1: 'type',
		    arg2: 'type2'
		 )  :
	'''
	comment
    	morecomments	
    	'''`,
	[]
    ).

test(subprog_python_b) :-
    subprog_python(
	dec("foo", " arg1: 'type', arg2: 'type2'", ""),
	`def foo (  arg1: 'type',
		    arg2: 'type2'
		 )  :
	more more more code`,
	`\n\tmore more more code`
    ).


:- end_tests(python).

